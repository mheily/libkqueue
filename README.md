debian-packages
===============

Several Debian packages are maintained within this repository.

They are:

 * libkqueue
 * libpthread_workqueue

Example of building and uploading a package:

	$ make clean package upload
	
